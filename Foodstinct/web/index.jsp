<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    
    <!--Custom Stylesheet -->
    <link href="dist/css/animate.css" rel="stylesheet">
    <link href="dist/css/style.css" rel="stylesheet">

    <title>Foodstinct</title>
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>

  </head>

  <body >
    <div class="container">
        <div class="top">
            <img src ="dist/images/logo2.png" style="display:block;margin-left:auto;margin-right:auto;padding-top:10px; ">
        </div>
        
        <div class="login-box animated fadeInUp">
            <div class ="box-header">
                <h2>Log In</h2>
            </div>
            
            <%
                String username = (String) session.getAttribute("username");

                if (username != null && username.equals("")) {
            %>
                    <p style="color:red">
                        <strong>Oh no!</strong> Invalid username/password
                    </p>
            <%
                } else if (username != null) {
                    response.sendRedirect("/Foodstinct/InventoryServlet");
                }
            %>
                
            <form action="LoginServlet" method="post">
                <label for="username">Username</label>
                <br/>
                <input type="text" name="username" placeholder="e.g. albert.tan.2016" required>
                <br/>
                <label for="password">Password</label>
                <br/>
                <input type="password" name="password" placeholder="***********" required>
                <br/>
                <button type="submit">Sign In</button>
                <a href="#"><p class="small">New Member?</p></a>
            </form>      
        </div>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
