CREATE DATABASE  IF NOT EXISTS `foodstinct` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `foodstinct`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: foodstinct
-- ------------------------------------------------------
-- Server version	5.7.9

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alertnotification`
--

DROP TABLE IF EXISTS `alertnotification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alertnotification` (
  `alertId` int(11) NOT NULL AUTO_INCREMENT,
  `ownerId` int(11) NOT NULL,
  `foodId` int(11) NOT NULL,
  `notified` int(11) DEFAULT '0',
  PRIMARY KEY (`alertId`),
  KEY `fk_alertNotification_ownerId_idx` (`ownerId`),
  KEY `fk_alertNotification_food_idx` (`foodId`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alertnotification`
--

LOCK TABLES `alertnotification` WRITE;
/*!40000 ALTER TABLE `alertnotification` DISABLE KEYS */;
INSERT INTO `alertnotification` VALUES (39,2,4,1),(38,2,6,1),(37,2,11,1),(36,2,8,1);
/*!40000 ALTER TABLE `alertnotification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food`
--

DROP TABLE IF EXISTS `food`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food` (
  `foodId` int(11) NOT NULL AUTO_INCREMENT,
  `foodName` varchar(45) NOT NULL,
  `foodCatId` int(11) NOT NULL,
  PRIMARY KEY (`foodId`),
  KEY `fk_foodCat_idx` (`foodCatId`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food`
--

LOCK TABLES `food` WRITE;
/*!40000 ALTER TABLE `food` DISABLE KEYS */;
INSERT INTO `food` VALUES (1,'Apple',1),(2,'Grapes',1),(3,'Spinach',2),(4,'Pear',1),(5,'Eggs',3),(6,'Milk',3),(7,'Asparagus',2),(8,'Bacon',5),(9,'Tomatoes',2),(10,'Apple Juice',4),(11,'Yogurt',3);
/*!40000 ALTER TABLE `food` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foodcat`
--

DROP TABLE IF EXISTS `foodcat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foodcat` (
  `foodCatId` int(11) NOT NULL AUTO_INCREMENT,
  `foodCat` varchar(45) NOT NULL,
  PRIMARY KEY (`foodCatId`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foodcat`
--

LOCK TABLES `foodcat` WRITE;
/*!40000 ALTER TABLE `foodcat` DISABLE KEYS */;
INSERT INTO `foodcat` VALUES (1,'Fruits'),(2,'Vegetables'),(3,'Dairy Products'),(4,'Drinks'),(5,'Meat'),(6,'Fish');
/*!40000 ALTER TABLE `foodcat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foodshare`
--

DROP TABLE IF EXISTS `foodshare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foodshare` (
  `ownerId` int(11) NOT NULL,
  `shareWith` int(11) NOT NULL,
  `foodId` int(11) DEFAULT NULL,
  `dateShared` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approved` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ownerId`,`shareWith`,`dateShared`),
  KEY `fk_foodShare_foodId_idx` (`foodId`),
  KEY `fk_foodShare_shareWith_idx` (`shareWith`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foodshare`
--

LOCK TABLES `foodshare` WRITE;
/*!40000 ALTER TABLE `foodshare` DISABLE KEYS */;
INSERT INTO `foodshare` VALUES (2,4,10,'2016-04-20 19:59:54',1),(2,5,9,'2016-04-20 20:00:57',-1),(2,5,1,'2016-04-20 20:06:40',-1),(2,5,4,'2016-04-20 20:06:46',1),(2,3,6,'2016-04-20 20:24:08',1),(8,2,9,'2016-04-20 20:25:06',0),(8,2,9,'2016-04-20 20:26:54',0),(4,2,7,'2016-04-20 20:39:50',0),(3,2,9,'2016-04-20 21:37:55',1),(2,3,10,'2016-04-20 21:39:10',-1),(6,2,11,'2016-04-20 21:41:09',1);
/*!40000 ALTER TABLE `foodshare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `owner`
--

DROP TABLE IF EXISTS `owner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `owner` (
  `ownerId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`ownerId`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `owner`
--

LOCK TABLES `owner` WRITE;
/*!40000 ALTER TABLE `owner` DISABLE KEYS */;
INSERT INTO `owner` VALUES (1,'kimyong','password'),(2,'gwen','password'),(3,'mark','password'),(4,'samiul','password'),(5,'yein','password'),(6,'sherlyn','password'),(7,'geraldine','password'),(8,'arnold','password');
/*!40000 ALTER TABLE `owner` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-20 23:47:13
