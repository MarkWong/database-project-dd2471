package servlet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPServiceProvider;
import com.google.gson.Gson;
import foodstinct.FoodItemFeedGenerator;
import foodstinct.dao.AlertNotificationDAO;
import foodstinct.dao.FoodDAO;
import foodstinct.dao.OwnerDAO;
import foodstinct.eventbean.InventoryDataResult;
import foodstinct.monitor.FoodstinctItemResultListener;
import foodstinct.monitor.FoodstinctFeedMonitor;
import foodstinct.eventbean.ItemFeed;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;

/**
 *
 * @author gwennisme
 */
public class InventoryServlet extends HttpServlet {
    
    ArrayList<InventoryDataResult> newData = new ArrayList<InventoryDataResult>();
    ArrayList<String> expiringItems = new ArrayList<String>();
    String username = "";
    int userId = -1;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession();
        username = (String) session.getAttribute("username");
        userId = (Integer) session.getAttribute("ownerId");
        
        if (username == null) {
            response.sendRedirect("/Foodstinct/index.jsp");
        }
        
        String refresh = (String) request.getParameter("refresh");
        // Clear all expiring items because you do not need the alert to be displayed again
        expiringItems.clear();
        if (refresh == null || !refresh.equals("false")) {
            // Refresh the data whenever required
            newData.clear();

            Configuration configuration = new Configuration();
            configuration.addEventType("ItemFeed", ItemFeed.class.getName());

            System.out.println("Setting up EPL");
            EPServiceProvider epService = EPServiceProviderManager.getProvider("Foodstinct", configuration);
            epService.initialize();
            new FoodstinctFeedMonitor(epService, new FoodstinctItemResultListener(), this);

            FoodItemFeedGenerator generator = new FoodItemFeedGenerator();
            LinkedList<ItemFeed> stream = generator.makeEventStream();
            System.out.println("Generating test events");

            System.out.println("Sending " + stream.size() + " events");

            for (Object theEvent : stream) {
                epService.getEPRuntime().sendEvent(theEvent);
            }

            System.out.println("Done.");
        }
        
        // Convert the new data received into JSON for display in JSP
        Gson gson = new Gson();
        String data = gson.toJson(newData);

        // Tabulate the list of items that are going to expire
        String expiring = "";
        if (expiringItems.size() > 0) {
            for (int i = 0; i < expiringItems.size(); i++) {
                expiring += expiringItems.get(i) + ", ";
            }
        }
        if (expiring.length() > 2) {
            expiring = expiring.substring(0, expiring.length() - 2);
        }

        request.setAttribute("data", data);
        request.setAttribute("username", username);
        request.setAttribute("expiring", expiring);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/myinventory.jsp");
        requestDispatcher.forward(request, response);
    }
    
    public void sendNewData(ItemFeed itemFeed) {
        System.out.println(".update Received: " + itemFeed);
        
        // Retrieve food category
        FoodDAO foodDAO = new FoodDAO();
        String foodCat = foodDAO.retrieveFoodCategory(itemFeed.getFoodName());
        
        // Retrieve owner's username
        OwnerDAO ownerDAO = new OwnerDAO();
        String inputUsername = ownerDAO.retrieveUsername(Integer.parseInt(itemFeed.getUserId()));
        
        // Process the input stream data
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        
        AlertNotificationDAO alertDAO = new AlertNotificationDAO();
        
        try {
            Date today = new Date();
            Date expiryDate = dateFormatter.parse(itemFeed.getExpiryDate());

            long diff = expiryDate.getTime() - today.getTime();
            int daysToExpiry = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + 1;
            
            // change the number of days later
            if (inputUsername.equals(username) && daysToExpiry <= 3) {
                expiringItems.add(itemFeed.getFoodName());
                int foodId = foodDAO.retrieveFoodId(itemFeed.getFoodName());
                alertDAO.insertData(userId, foodId);
            }
            
            InventoryDataResult data = new InventoryDataResult(Integer.parseInt(itemFeed.getUserId()), inputUsername, itemFeed.getFoodName(), foodCat, itemFeed.getExpiryDate(), daysToExpiry);
            newData.add(data);
            
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
