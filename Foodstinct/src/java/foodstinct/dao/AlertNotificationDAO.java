/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodstinct.dao;

import foodstinct.ConnectionManager;
import foodstinct.eventbean.Request;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author gwennisme
 */
public class AlertNotificationDAO {
    private static final String ALERT_NOTIFICATION_TABLE = "alertNotification";
    
    public void insertData(int ownerId, int foodId) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ConnectionManager connMgr = new ConnectionManager();
        
        try {
            conn = connMgr.getConnection();
            
            String query = "INSERT INTO " + ALERT_NOTIFICATION_TABLE + " (ownerId, foodId) VALUES (?, ?)";

            stmt = conn.prepareStatement(query);
            
            stmt.setInt(1, ownerId);
            stmt.setInt(2, foodId);

            stmt.executeUpdate();

        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }  
    
    public void updateAlert(int ownerId, int foodId) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ConnectionManager connMgr = new ConnectionManager();
        
        try {
            conn = connMgr.getConnection();
            
            String query = "UPDATE " + ALERT_NOTIFICATION_TABLE + " SET notified = ? WHERE ownerId = ? AND foodId = ?";

            stmt = conn.prepareStatement(query);
            
            stmt.setInt(1, 1);
            stmt.setInt(2, ownerId);
            stmt.setInt(3, foodId);
            
            stmt.executeUpdate();

        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }
    
    public String getAlertMessage (int ownerId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String expiringItems = "";
        ConnectionManager connMgr = new ConnectionManager();
        
        try {
            conn = connMgr.getConnection();
            String sql = "select foodName from alertnotification a\n" +
                        "inner join food f on a.foodId = f.foodId\n" +
                        "where a.notified = 0";
            stmt = conn.prepareStatement(sql);
 
            rs = stmt.executeQuery();
 
            while (rs.next()) {
                expiringItems += rs.getString(1) + ", ";
            }
            
            if (!expiringItems.equals("")) {
                expiringItems = expiringItems.substring(0, expiringItems.length() - 2);
            }
            
            return expiringItems;
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            connMgr.close(conn, stmt, rs);
        }
        return expiringItems;
    }
}
