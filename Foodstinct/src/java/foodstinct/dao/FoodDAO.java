/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodstinct.dao;

import foodstinct.ConnectionManager;
import foodstinct.eventbean.Request;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author gwennisme
 */
public class FoodDAO {
    private static final String FOOD_TABLE = "food";
    private static final String FOODCAT_TABLE = "foodcat";
    private static final String FOOD_SHARE_TABLE = "foodshare";
    
    public String retrieveFoodCategory(String foodName) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String foodCategory = "";
        ConnectionManager connMgr = new ConnectionManager();
        try {
            conn = connMgr.getConnection();
            String sql = "select fc.foodcat from food f\n" +
                        "inner join foodcat fc on f.foodCatId = fc.foodCatId\n" +
                        "where f.foodName = ?";
            stmt = conn.prepareStatement(sql);
 
            stmt.setString(1, foodName);
 
            rs = stmt.executeQuery();
 
            while (rs.next()) {
                foodCategory = rs.getString(1);
            }
            
            return foodCategory;
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            connMgr.close(conn, stmt, rs);
        }
        return foodCategory;
    }
    
    public int retrieveFoodId(String foodName) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int foodId = -1;
        ConnectionManager connMgr = new ConnectionManager();
        try {
            conn = connMgr.getConnection();
            String sql = "SELECT foodId FROM food where foodName = ?;";
            stmt = conn.prepareStatement(sql);
 
            stmt.setString(1, foodName);
 
            rs = stmt.executeQuery();
 
            while (rs.next()) {
                foodId = rs.getInt(1);
            }
            
            return foodId;
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            connMgr.close(conn, stmt, rs);
        }
        return foodId;
    }
    
    public void requestFood(int shareWithId, int ownerId, int foodId) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ConnectionManager connMgr = new ConnectionManager();
        
        try {
            conn = connMgr.getConnection();
            
            String query = "INSERT INTO " + FOOD_SHARE_TABLE + " (ownerId, shareWith, foodId) VALUES (?, ?, ?)";

            stmt = conn.prepareStatement(query);
            
            stmt.setInt(1, ownerId);
            stmt.setInt(2, shareWithId);
            stmt.setInt(3, foodId);

            stmt.executeUpdate();

        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }
    
    public ArrayList<Request> getFoodByOwnerId (int ownerId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<Request> requestList = new ArrayList<Request>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ConnectionManager connMgr = new ConnectionManager();
        
        try {
            conn = connMgr.getConnection();
            String sql = "SELECT owner.username, food.foodName, foodShare.dateShared, foodshare.approved FROM foodshare, owner, food where foodshare.shareWith = ? \n" +
                         "and food.foodId = foodshare.foodId and owner.ownerId = foodshare.ownerId;";
            
           
            //SELECT owner.username, food.foodName, foodShare.dateShared FROM foodshare, owner, food where foodshare.shareWith = 3 and food.foodId = foodshare.foodId and owner.ownerId = foodshare.ownerId;

            stmt = conn.prepareStatement(sql);
 
            stmt.setInt(1, ownerId);
 
            rs = stmt.executeQuery();
 
            while (rs.next()) {
                Request r = new Request(rs.getString(1), rs.getString(2), dateFormat.format(rs.getTimestamp(3)), rs.getInt(4));
                requestList.add(r);
            }
            
            return requestList;
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            connMgr.close(conn, stmt, rs);
        }
        return requestList;
    }
    
    public void updateRequestStatus(int ownerId, int shareWithId, int foodId, Timestamp dateShared, int status) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ConnectionManager connMgr = new ConnectionManager();
        
        try {
            conn = connMgr.getConnection();
            
            String query = "update " + FOOD_SHARE_TABLE + " set approved = ? \n" +
                            "where ownerId = ? and shareWith = ? and foodId = ? "+ 
                            "and dateShared = ?";

            stmt = conn.prepareStatement(query);
            
            stmt.setInt(1, status);
            stmt.setInt(2, ownerId);
            stmt.setInt(3, shareWithId);
            stmt.setInt(4, foodId);
            stmt.setTimestamp(5, dateShared);

            stmt.executeUpdate();

        } catch (SQLException e) {
          e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    public ArrayList<Request> getRequestNotification(int ownerId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<Request> requestList = new ArrayList<Request>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ConnectionManager connMgr = new ConnectionManager();
        
        try {
            conn = connMgr.getConnection();
            String sql = "select o.username, f.foodName, fs.dateShared,fs.approved from foodshare fs\n" +
                        "inner join food f on fs.foodId = f.foodId\n" +
                        "inner join owner o on fs.shareWith = o.ownerId\n" +
                        "where fs.ownerId = ?\n" +
                        "order by fs.dateShared desc;";
            stmt = conn.prepareStatement(sql);
 
            stmt.setInt(1, ownerId);
 
            rs = stmt.executeQuery();
 
            while (rs.next()) {
                Request r = new Request(rs.getString(1), rs.getString(2), dateFormat.format(rs.getTimestamp(3)), rs.getInt(4));
                requestList.add(r);
            }
            
            return requestList;
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            connMgr.close(conn, stmt, rs);
        }
        return requestList;
    }
    
    public ArrayList<Request> getMyRequestList (int ownerId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<Request> requestList = new ArrayList<Request>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ConnectionManager connMgr = new ConnectionManager();
        
        try {
            conn = connMgr.getConnection();
            String sql = "select o.username, f.foodName, fs.dateShared,fs.approved from foodshare fs\n" +
                        "inner join food f on fs.foodId = f.foodId\n" +
                        "inner join owner o on fs.ownerId = o.ownerId\n" +
                        "where fs.shareWith = ?\n" +
                        "order by fs.dateShared desc;";
            stmt = conn.prepareStatement(sql);
 
            stmt.setInt(1, ownerId);
 
            rs = stmt.executeQuery();
 
            while (rs.next()) {
                Request r = new Request(rs.getString(1), rs.getString(2), dateFormat.format(rs.getTimestamp(3)), rs.getInt(4));
                requestList.add(r);
            }
            
            return requestList;
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            connMgr.close(conn, stmt, rs);
        }
        return requestList;
    }
    
    public ArrayList<Request> getMyRequestMessage (int ownerId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<Request> requestList = new ArrayList<Request>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ConnectionManager connMgr = new ConnectionManager();
        
        try {
            conn = connMgr.getConnection();
            String sql = "select o.username, f.foodName, fs.dateShared,fs.approved from foodshare fs \n" +
                        "inner join food f on fs.foodId = f.foodId\n" +
                        "inner join owner o on fs.ownerId = o.ownerId\n" +
                        "where fs.shareWith = ? and date(dateShared) = curdate()";
            stmt = conn.prepareStatement(sql);
 
            stmt.setInt(1, ownerId);
 
            rs = stmt.executeQuery();
 
            while (rs.next()) {
                Request r = new Request(rs.getString(1), rs.getString(2), dateFormat.format(rs.getTimestamp(3)), rs.getInt(4), 0);
                requestList.add(r);
            }
            
            return requestList;
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            connMgr.close(conn, stmt, rs);
        }
        return requestList;
    }
    
    public ArrayList<Request> getFriendRequestMessage (int ownerId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<Request> requestList = new ArrayList<Request>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ConnectionManager connMgr = new ConnectionManager();
        
        try {
            conn = connMgr.getConnection();
            String sql = "select o.username as friendName, f.foodName, fs.dateShared,fs.approved from foodshare fs\n" +
                            "inner join food f on fs.foodId = f.foodId\n" +
                            "inner join owner o on fs.shareWith = o.ownerId\n" +
                            "where fs.ownerId = ? and date(dateShared) = curdate()";
            stmt = conn.prepareStatement(sql);
 
            stmt.setInt(1, ownerId);
 
            rs = stmt.executeQuery();
 
            while (rs.next()) {
                Request r = new Request(rs.getString(1), rs.getString(2), dateFormat.format(rs.getTimestamp(3)), rs.getInt(4), 1);
                requestList.add(r);
            }
            
            return requestList;
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            connMgr.close(conn, stmt, rs);
        }
        return requestList;
    }
}
