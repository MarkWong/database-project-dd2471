/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodstinct.dao;

import foodstinct.ConnectionManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author gwennisme
 */
public class OwnerDAO {
    
    private static final String OWNER_TABLE = "owner";
    
    public int authenticateUser(String username, String password) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int ownerId = -1;
        ConnectionManager connMgr = new ConnectionManager();
        try {
            conn = connMgr.getConnection();
            String sql = "Select ownerId FROM " + OWNER_TABLE + " WHERE username = ? AND password = ?";
            stmt = conn.prepareStatement(sql);
 
            stmt.setString(1, username);
            stmt.setString(2, password);
 
            rs = stmt.executeQuery();
 
            while (rs.next()) {
                ownerId = rs.getInt(1);
            }
 
            // If the username and password is valid, it will return a count of 1 which means that it is a valid login
            if (ownerId != -1) {
                return ownerId;
            }
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            connMgr.close(conn, stmt, rs);
        }
        return ownerId;
    }
    
    public String retrieveUsername(int userId) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String username = "";
        ConnectionManager connMgr = new ConnectionManager();
        try {
            conn = connMgr.getConnection();
            String sql = "select username from owner where ownerId = ?";
            
            stmt = conn.prepareStatement(sql);
 
            stmt.setInt(1, userId);
 
            rs = stmt.executeQuery();
 
            while (rs.next()) {
                username = rs.getString(1);
            }
            
            return username;
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            connMgr.close(conn, stmt, rs);
        }
        return username;
    }
    
    public int retrieveUserId(String username) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int userId = -1;
        ConnectionManager connMgr = new ConnectionManager();
        try {
            conn = connMgr.getConnection();
            String sql = "select ownerId from owner where username = ?";
            
            stmt = conn.prepareStatement(sql);
 
            stmt.setString(1, username);
 
            rs = stmt.executeQuery();
 
            while (rs.next()) {
                userId = rs.getInt(1);
            }
            
            return userId;
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            connMgr.close(conn, stmt, rs);
        }
        return userId;
    }
}
