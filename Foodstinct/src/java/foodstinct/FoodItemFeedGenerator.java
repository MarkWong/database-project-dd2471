/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodstinct;

import java.util.LinkedList;
import foodstinct.eventbean.ItemFeed;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.Date;
/**
 *
 * @author gwennisme
 */
public class FoodItemFeedGenerator {
    public LinkedList<ItemFeed> makeEventStream () {
        LinkedList<ItemFeed> stream = new LinkedList<ItemFeed>();

        String[] foodName = {"Apple", "Grapes", "Spinach", "Pear", "Eggs", "Milk", "Asparagus", "Bacon", "Tomatoes", "Apple Juice", "Yogurt"};
        
        Random rand = new Random();
        
        String[] expiryDate = {"2016-04-20", "2016-04-18", "2016-05-17"};

        for (int i = 0; i < 10; i++) {
            // Generate a random user id from 0 to 8
            int userId = rand.nextInt(8) + 1;
            int foodId = rand.nextInt(foodName.length);
            int day = rand.nextInt(30) + 1;
            int month = 5;
            
            if (day > 19) {
                month = 4;
            }
            
            String dateStr = "2016-0" + month + "-" + day;
            
            try {
                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = dateFormatter.parse(dateStr);
                dateStr = dateFormatter.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            
            stream.add(new ItemFeed(Integer.toString(userId), foodName[foodId], dateStr));
        }

        return stream;
    }
}
