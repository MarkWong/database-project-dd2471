/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodstinct.eventbean;

/**
 *
 * @author gwennisme
 */
public class InventoryDataResult {
    private int shelfId;
    private String username;
    private String foodName;
    private String foodCategory;
    private String expiryDate;
    private int daysToExpiry;
    
    public InventoryDataResult(int shelfId, String username, String foodName, String foodCategory, String expiryDate, int daysToExpiry) {
        this.shelfId = shelfId;
        this.username = username;
        this.foodName = foodName;
        this.foodCategory = foodCategory;
        this.expiryDate = expiryDate;
        this.daysToExpiry = daysToExpiry;
    }
    
    public int getShelfId() {
        return shelfId;
    }
    
    public String getUsername() {
        return username;
    }
    
    public String getFoodName() {
        return foodName;
    }
    
    public String getFoodCategory() {
        return foodCategory;
    }
    
    public String getExpiryDate() {
        return expiryDate;
    }
    
    public int getDaysToExpiry() {
        return daysToExpiry;
    }
    
    public String toString() {
        return "Username: " + username + ", Food Name: " + foodName + ", Food Category: " + foodCategory + ", Expiry Date: " + expiryDate + ", Days To Expiry: " + daysToExpiry;
    }
}
