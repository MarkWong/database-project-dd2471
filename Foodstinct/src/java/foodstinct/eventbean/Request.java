/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodstinct.eventbean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author gwennisme
 */
public class Request implements Comparable<Request> {
    private String requester;
    private String foodItem;
    private String dateRequested;
    private int approvalStatus; // 0 - pending, 1 - approved, -1 - rejected
    private int requestType; // 0 - my request, 1 - friend request
    
    public Request(String requester, String foodItem, String dateRequested, int approvalStatus) {
        this.requester = requester;
        this.foodItem = foodItem;
        this.dateRequested = dateRequested;
        this.approvalStatus = approvalStatus;
    }
    
    public Request (String requester, String foodItem, String dateRequested, int approvalStatus, int requestType) {
        this(requester, foodItem, dateRequested, approvalStatus);
        this.requestType = requestType;
    }
    
    public String getRequester() {
        return requester;
    }
    
    public String getFoodItem() {
        return foodItem;
    }
    
    public String getDateRequested() {
        return dateRequested;
    }
    
    public int getApprovalStatus() {
        return approvalStatus;
    }
    
    public int getRequestType() {
        return requestType;
    }
    
    public int compareTo(Request r1) {
        return (r1.getDateRequested().compareTo(dateRequested));
    }
}
