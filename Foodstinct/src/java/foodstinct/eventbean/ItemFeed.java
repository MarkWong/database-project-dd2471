/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodstinct.eventbean;
/**
 *
 * @author gwennisme
 */
public class ItemFeed {
    private String userId;
    private String foodName;
    private String expiryDate;

    public ItemFeed(String userId, String foodName, String expiryDate) {
            this.userId = userId;
            this.foodName = foodName;
            this.expiryDate = expiryDate;
    }

    public String getUserId() {
            return userId;
    }

    public String getFoodName() {
            return foodName;
    }

    public String getExpiryDate() {
            return expiryDate;
    }

    public String toString() {
            return "User ID: " + userId + ", Food Name: " + foodName + ", Expiry Date: " + expiryDate;
    }
}
