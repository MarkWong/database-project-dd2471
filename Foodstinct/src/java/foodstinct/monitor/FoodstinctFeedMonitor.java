/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodstinct.monitor;

import servlet.InventoryServlet;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;
import foodstinct.eventbean.ItemFeed;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;
import java.text.ParseException;

/**
 *
 * @author gwennisme
 */
public class FoodstinctFeedMonitor {
    private final EPServiceProvider epService;
    private final FoodstinctItemResultListener foodstinctItemResultListener;
    private final InventoryServlet inventoryServlet;

    public FoodstinctFeedMonitor(EPServiceProvider epService, final FoodstinctItemResultListener foodstinctItemResultListener, final InventoryServlet inventoryServlet) {
        this.epService = epService;
        this.foodstinctItemResultListener = foodstinctItemResultListener;
        this.inventoryServlet = inventoryServlet;

        String expressionText = "every itemfeed=ItemFeed()";
        EPStatement factory = epService.getEPAdministrator().createPattern(expressionText);

        factory.addListener(new UpdateListener() {
                public void update(EventBean[] newEvents, EventBean[] oldEvents) {
                    ItemFeed itemFeed = (ItemFeed) newEvents[0].get("itemfeed");
                    inventoryServlet.sendNewData(itemFeed);
                }
        });
    }
}
