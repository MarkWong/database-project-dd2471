/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foodstinct.monitor;

import java.util.List;
import java.util.Collections;
import java.util.LinkedList;
import foodstinct.eventbean.ItemFeed;
/**
 *
 * @author gwennisme
 */
public class FoodstinctItemResultListener {
    private List<Object> matchEvents = Collections.synchronizedList(new LinkedList<Object>());

    public void emitted(ItemFeed object)
    {
        System.out.println(".emitted Received emitted " + object);
        matchEvents.add(object);
    }

    public int getSize()
    {
        return matchEvents.size();
    }

    public List getMatchEvents()
    {
        return matchEvents;
    }

    public void clearMatched()
    {
        matchEvents.clear();
    }
}
