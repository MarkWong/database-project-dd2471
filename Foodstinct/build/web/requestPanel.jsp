<%-- 
    Document   : requestPanel
    Created on : Apr 20, 2016, 10:08:33 PM
    Author     : gwennisme
--%>

<%@page import="foodstinct.dao.AlertNotificationDAO"%>
<%@page import="java.util.Collections"%>
<%@page import="foodstinct.dao.FoodDAO"%>
<%@page import="com.google.gson.reflect.TypeToken"%>
<%@page import="java.lang.reflect.Type"%>
<%@page import="foodstinct.eventbean.Request"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Food Request Panel</title>

        <!-- Bootstrap core CSS -->
        <link href="dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="dist/css/sb-admin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="dist/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="dist/js/jquery-1.12.3.min.js"></script>
        <script type="text/javascript" src="dist/js/bootstrap.js"></script>


    </head>
    <body style="background-color: white;">

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/Foodstinct/InventoryServlet"><font color="#1b95e0">FOOD</font>STINCT</a>
                </div>

                <!--Message Alert-->
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                        <ul class="dropdown-menu message-dropdown">
                            <%
                                int ownerId = (Integer) session.getAttribute("ownerId");

                                boolean noMsg = true;

                                AlertNotificationDAO alertDAO = new AlertNotificationDAO();
                                String expiringMsg = alertDAO.getAlertMessage(ownerId);

                                if (!expiringMsg.equals("")) {
                                    noMsg = false;
                            %>
                            <li class="message-preview">
                                <a href="#">
                                    <div class="media">
                                        <span class="pull-left">
                                            <img class="media-object" src="http://placehold.it/50x50" alt="">
                                        </span>
                                        <div class="media-body">
                                            <h5 class="media-heading"><strong>Foodstinct</strong>
                                            </h5>
                                            <p class="small text-muted"><i class="fa fa-clock-o"></i> Latest</p>
                                            <p>Your <%=expiringMsg%> is going to expire soon! Please use it!</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <%
                                }

                                FoodDAO foodDAO = new FoodDAO();
                                ArrayList<Request> myRequestList = foodDAO.getMyRequestMessage(ownerId);
                                ArrayList<Request> friendRequestList = foodDAO.getFriendRequestMessage(ownerId);

                                ArrayList<Request> messageList = new ArrayList<>();
                                messageList.addAll(myRequestList);
                                messageList.addAll(friendRequestList);

                                if (messageList.size() > 0) {
                                    noMsg = false;
                                    Collections.sort(messageList);

                                    for (Request r : messageList) {
                                        String messageFrom = "";
                                        String message = "";
                                        String linkTo = "";
                                        if (r.getRequestType() == 0) { // my request
                                            messageFrom = "Foodstinct";
                                            linkTo = "/Foodstinct/MyRequestServlet";
                                            if (r.getApprovalStatus() == 0) {
                                                message = "Your request for " + r.getFoodItem() + " is pending confirmation from " + r.getRequester();
                                            } else if (r.getApprovalStatus() == 1) {
                                                message = "Your request for " + r.getFoodItem() + " has been approved by " + r.getRequester();
                                            } else {
                                                message = "Your request for " + r.getFoodItem() + " has been rejected by " + r.getRequester();
                                            }
                                        } else { // friend request
                                            messageFrom = r.getRequester();
                                            linkTo = "/Foodstinct/RequestPanelServlet";
                                            message = r.getRequester() + " requested for " + r.getFoodItem() + ". Would you give the item to him/her?";
                                        }
                            %>
                            <li class="message-preview">
                                <a href="<%=linkTo%>">
                                    <div class="media">
                                        <span class="pull-left">
                                            <img class="media-object" src="http://placehold.it/50x50" alt="">
                                        </span>
                                        <div class="media-body">
                                            <h5 class="media-heading"><strong><%=messageFrom%></strong>
                                            </h5>
                                            <p class="small text-muted"><i class="fa fa-clock-o"></i> <%=r.getDateRequested()%></p>
                                            <p><%=message%></p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <%
                                    }
                                }
                                if (noMsg) {
                            %>
                                    <li class="message-footer">
                                        No New Messages
                                    </li>
                            <%
                                }
                            %>
                        </ul>
                    </li>
                    <!--End of MessageAlert-->

                    <%
                        String username = (String) session.getAttribute("username");
                        username = username.toLowerCase();
                        username = username.substring(0, 1).toUpperCase() + username.substring(1);
                    %>

                    <!--Username-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <%=username%> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="/Foodstinct/LogoutServlet"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="/Foodstinct/InventoryServlet"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="/Foodstinct/MyRequestServlet"><i class="fa fa-fw fa-edit"></i> Item Requests</a>
                        </li>
                        <li class="active">
                            <a href="/Foodstinct/RequestPanelServlet"><i class="fa fa-fw fa-edit"></i> Friend's Requests</a>
                        </li>
                    </ul>
                </div>
            </nav>    

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <br/>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-fw fa-table"></i> Friend's Request Panel
                                </div>

                                <div class="panel-body">

                                    <%
                                        String data = (String) request.getAttribute("data");
                                        Gson gson = new Gson();
                                        Type type = new TypeToken<ArrayList<Request>>() {
                                        }.getType();
                                        ArrayList<Request> listOfRequests = gson.fromJson(data, type);

                                        if (listOfRequests.size() > 0) {
                                            int countOfNewRequests = 0;
                                            for (int i = 0; i < listOfRequests.size(); i++) {
                                                if (listOfRequests.get(i).getApprovalStatus() == 0) {
                                                    countOfNewRequests++;
                                                }
                                            }

                                            if (countOfNewRequests > 0) {
                                    %>

                                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <strong>Heads up!</strong> You have <%=countOfNewRequests%> new requests for food items!
                                    </div>

                                    <%
                                        }
                                    %>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Requester</th>
                                                <th>Request Info</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <%
                                                for (int i = 0; i < listOfRequests.size(); i++) {
                                            %>
                                            <tr>
                                                <th scope="row"><%=i + 1%></th>
                                                <td><%=listOfRequests.get(i).getRequester()%></td>
                                                <td><%=listOfRequests.get(i).getRequester()%> requested for <%=listOfRequests.get(i).getFoodItem()%> on <%=listOfRequests.get(i).getDateRequested()%></td>
                                                <td>
                                                    <%
                                                        if (listOfRequests.get(i).getApprovalStatus() == 0) {
                                                            // Display button to approve/reject
                                                    %>
                                                    <button type="button" class="btn btn-success-outline" onclick="location.href = 'approveRequest.jsp?state=approve&requester=<%=listOfRequests.get(i).getRequester()%>&foodName=<%=listOfRequests.get(i).getFoodItem()%>&timestamp=<%=listOfRequests.get(i).getDateRequested()%>'">&#10004;</button>
                                                    <button type="button" class="btn btn-danger-outline" onclick="location.href = 'approveRequest.jsp?state=reject&requester=<%=listOfRequests.get(i).getRequester()%>&foodName=<%=listOfRequests.get(i).getFoodItem()%>&timestamp=<%=listOfRequests.get(i).getDateRequested()%>'">&#10006;</button>
                                                    <%
                                                        } else if (listOfRequests.get(i).getApprovalStatus() == 1) {
                                                            // Display text to show request has been approved
                                                            out.println("Approved");
                                                        } else {
                                                            // Display text to show request has been rejected
                                                            out.println("Rejected");
                                                        }
                                                    %>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </tbody>
                                    </table>
                                    <%
                                        } else {
                                            out.println("No new requests made!");
                                        }
                                    %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!--End of Wrapper-->
    </body>
</html>
