<%-- 
    Document   : approveRequest
    Created on : Apr 20, 2016, 11:09:45 PM
    Author     : gwennisme
--%>

<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="foodstinct.dao.OwnerDAO"%>
<%@page import="foodstinct.dao.FoodDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String requestStatus = request.getParameter("state");
    String requesterName = request.getParameter("requester");
    String foodName = request.getParameter("foodName");
    String timestampStr = request.getParameter("timestamp");
    
    Integer ownerId = (Integer) session.getAttribute("ownerId");
    
    FoodDAO foodDAO = new FoodDAO();
    int foodId = foodDAO.retrieveFoodId(foodName.trim());
    
    OwnerDAO ownerDAO = new OwnerDAO();
    int requesterId = ownerDAO.retrieveUserId(requesterName);
    
    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    Date parseDate = formatter.parse(timestampStr);
    Timestamp timestamp = new Timestamp(parseDate.getTime());
    
    if (requestStatus.equals("approve")) {
        // user approve the request
        foodDAO.updateRequestStatus(ownerId, requesterId, foodId, timestamp, 1);
    } else if (requestStatus.equals("reject")){
        // user reject the request
        foodDAO.updateRequestStatus(ownerId, requesterId, foodId, timestamp, -1);
    }
    
    response.sendRedirect("/Foodstinct/RequestPanelServlet");
%>
