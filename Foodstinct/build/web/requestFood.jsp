<%-- 
    Document   : requestFood
    Created on : Apr 20, 2016, 6:07:26 PM
    Author     : gwennisme
--%>

<%@page import="foodstinct.dao.OwnerDAO"%>
<%@page import="foodstinct.dao.FoodDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
<%
    int shareWithId = (Integer) session.getAttribute("ownerId");
    String foodName = request.getParameter("foodName");
    String ownerName = request.getParameter("username");
    
    FoodDAO foodDAO = new FoodDAO();
    int foodId = foodDAO.retrieveFoodId(foodName.trim());
    
    OwnerDAO ownerDAO = new OwnerDAO();
    int ownerId = ownerDAO.retrieveUserId(ownerName);
    
    foodDAO.requestFood(shareWithId, ownerId, foodId);
    
    response.sendRedirect("/Foodstinct/InventoryServlet?refresh=false&request=success");
%>
</html>
