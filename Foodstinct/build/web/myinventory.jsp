<%-- 
    Document   : myinventory
    Created on : Apr 18, 2016, 11:17:34 AM
    Author     : gwennisme
--%>

<%@page import="java.util.Date"%>
<%@page import="java.util.Comparator"%>
<%@page import="foodstinct.dao.AlertNotificationDAO"%>
<%@page import="java.util.Collections"%>
<%@page import="foodstinct.eventbean.Request"%>
<%@page import="foodstinct.dao.FoodDAO"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.lang.reflect.Type"%>
<%@page import="com.google.gson.reflect.TypeToken"%>
<%@page import="foodstinct.eventbean.InventoryDataResult"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="foodstinct.eventbean.ItemFeed"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Code to make the page refresh every 60 seconds -->
        <meta http-equiv="refresh" content="15" />

        <title>My Inventory</title>

        <!-- Bootstrap core CSS -->
        <link href="dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="dist/css/sb-admin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="dist/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="dist/js/jquery-1.12.3.min.js"></script>
        <script type="text/javascript" src="dist/js/bootstrap.js"></script>

        <script>
            $(document).ready(function () {
                console.log("Ready");
                $('.second').hide();
                $('.third').hide();
                $('.fourth').hide();
                (function ($) {
                    $('#filter').keyup(function () {

                        var rex = new RegExp($(this).val(), 'i');
                        $('.searchable tr').hide();
                        $('.searchable tr').filter(function () {
                            return rex.test($(this).text());
                        }).show();

                    });

                }(jQuery));
                
                (function ($) {
                    $('#favfriend').keyup(function () {

                        var rex = new RegExp($(this).val(), 'i');
                        $('.searchable tr').hide();
                        $('.searchable tr').filter(function () {
                            return rex.test($(this).text());
                        }).show();

                    });

                }(jQuery));

            });
            
            function viewAllUsers() {
                $('#filter').val('');
                refreshFilterBox();
            }
            
            function getExpiring3Food() {
                $('#filter').val('');
                refreshFilterBox2();
            }
            
            function getExpiring3Drinks() {
                $('#filter').val('');
                refreshFilterBox3();
            }
            
            function getFavouriteFriend() {
                $('#filter').val('');
                refreshFilterBox4();
            }

            function refreshFilterBox() {
                var rex = new RegExp($('#filter').val(), 'i');
                $('.second').hide();
                $('.third').hide();
                $('.fourth').hide();
                $('.first').filter(function () {
                    return rex.test($(this).text());
                }).show();
            }
            
            function refreshFilterBox2() {
                var rex = new RegExp($('#filter').val(), 'i');
                $('.second').filter(function () {
                    return rex.test($(this).text());
                }).show();
                $('.first').hide();
                $('.third').hide();
                $('.fourth').hide();
            }
            
            function refreshFilterBox3() {
                var rex = new RegExp($('#filter').val(), 'i');
                $('.third').filter(function () {
                    return rex.test($(this).text());
                }).show();
                $('.first').hide();
                $('.second').hide();
                $('.fourth').hide();
            }
            
            function refreshFilterBox4() {
                var rex = new RegExp($('#filter').val(), 'i');
                $('.fourth').filter(function () {
                    return rex.test($(this).text());
                }).show();
                $('.first').hide();
                $('.second').hide();
                $('.third').hide();

            }
        </script>
    </head>
    <body style="background-color: white;">
        <%

            String username = (String) session.getAttribute("username");
            username = username.toLowerCase();
            username = username.substring(0, 1).toUpperCase() + username.substring(1);
        %>
        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><font color="#1b95e0">FOOD</font>STINCT</a>
                </div>

                <!--Message Alert-->
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                        <ul class="dropdown-menu message-dropdown">
                            <%
                                int ownerId = (Integer) session.getAttribute("ownerId");
                                String favFriend = "";
                                boolean noMsg = true;

                                AlertNotificationDAO alertDAO = new AlertNotificationDAO();
                                String expiringMsg = alertDAO.getAlertMessage(ownerId);

                                if (!expiringMsg.equals("")) {
                                    noMsg = false;
                            %>
                            <li class="message-preview">
                                <a href="#">
                                    <div class="media">
                                        <span class="pull-left">
                                            <img class="media-object" src="http://placehold.it/50x50" alt="">
                                        </span>
                                        <div class="media-body">
                                            <h5 class="media-heading"><strong>Foodstinct</strong>
                                            </h5>
                                            <p class="small text-muted"><i class="fa fa-clock-o"></i> Latest</p>
                                            <p>Your <%=expiringMsg%> is going to expire soon! Please use it!</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <%
                                }

                                FoodDAO foodDAO = new FoodDAO();
                                ArrayList<Request> myRequestList = foodDAO.getMyRequestMessage(ownerId);
                                ArrayList<Request> friendRequestList = foodDAO.getFriendRequestMessage(ownerId);
                                out.println(myRequestList);
                                ArrayList<Request> messageList = new ArrayList<>();
                                messageList.addAll(myRequestList);
                                messageList.addAll(friendRequestList);

                                if (messageList.size() > 0) {
                                    noMsg = false;
                                    Collections.sort(messageList);

                                    for (Request r : messageList) {
                                        String messageFrom = "";
                                        String message = "";
                                        String linkTo = "";
                                        if (r.getRequestType() == 0) { // my request
                                            messageFrom = "Foodstinct";
                                            linkTo = "/Foodstinct/MyRequestServlet";
                                            if (r.getApprovalStatus() == 0) {
                                                message = "Your request for " + r.getFoodItem() + " is pending confirmation from " + r.getRequester();
                                            } else if (r.getApprovalStatus() == 1) {
                                                message = "Your request for " + r.getFoodItem() + " has been approved by " + r.getRequester();
                                            } else {
                                                message = "Your request for " + r.getFoodItem() + " has been rejected by " + r.getRequester();
                                            }
                                        } else { // friend request
                                            messageFrom = r.getRequester();
                                            linkTo = "/Foodstinct/RequestPanelServlet";
                                            message = r.getRequester() + " requested for " + r.getFoodItem() + ". Would you give the item to him/her?";
                                        }
                            %>
                            <li class="message-preview">
                                <a href="<%=linkTo%>">
                                    <div class="media">
                                        <span class="pull-left">
                                            <img class="media-object" src="http://placehold.it/50x50" alt="">
                                        </span>
                                        <div class="media-body">
                                            <h5 class="media-heading"><strong><%=messageFrom%></strong>
                                            </h5>
                                            <p class="small text-muted"><i class="fa fa-clock-o"></i> <%=r.getDateRequested()%></p>
                                            <p><%=message%></p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <%
                                    }
                                }
                                if (noMsg) {
                            %>
                                    <li class="message-footer">
                                        No New Messages
                                    </li>
                            <%
                                }
                            %>
                        </ul>
                    </li>
                    <!--End of MessageAlert-->

                    <!--Username-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <%=username%> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="/Foodstinct/LogoutServlet"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="/Foodstinct/InventoryServlet"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="/Foodstinct/MyRequestServlet"><i class="fa fa-fw fa-edit"></i> Item Requests</a>
                        </li>
                        <li>
                            <a href="/Foodstinct/RequestPanelServlet"><i class="fa fa-fw fa-edit"></i> Friend's Requests</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Dashboard <small>Statistics Overview</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li class="active">
                                    <i class="fa fa-dashboard"></i> Dashboard
                                </li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.row -->


                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-comments fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">
                                            <%=messageList.size()%>
                                                </div>
                                            <div>New Messages</div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="panel-footer">
                                    <span class="pull-left"><br></span>
                                    <span class="pull-right"><br></span>
                                    <div class="clearfix"></div>
                                </div>
                               
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-cutlery fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">Top 3</div>
                                            <div>Expiring food items</div>
                                            <!--<div>New Comments!</div>-->
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="panel-footer">
                                    <span class="pull-left" onclick="getExpiring3Food()">View Details</span>
                                    <span class="pull-right" onclick="getExpiring3Food()"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                               
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-glass fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">Top 3</div>
                                            <div>Expiring drink items</div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="panel-footer">
                                        <span class="pull-left" onclick="getExpiring3Drinks()">View Details</span>
                                        <span class="pull-right" onclick="getExpiring3Drinks()"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <i class="fa fa-child fa-5x"></i>
                                        </div>
                                        <div class="col-xs-10 text-right">
                                            <div class="huge">Food</div>
                                            <div>From favourite friend</div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="panel-footer">
                                        <span class="pull-left" onclick="getFavouriteFriend()">View Details</span>
                                        <span class="pull-right" onclick="getFavouriteFriend()"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                            </div>
                        </div>
                     
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-fw fa-table"></i> Hostel Fridge Inventory<br/></h3>
                                </div>

                                <div class="panel-body">
                                    <%
                                        String expiring = (String) request.getAttribute("expiring");
                                        if (expiring != null && !expiring.equals("")) {
                                    %>
                                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="updateAlert()">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <strong>Oh no!</strong> Your <%=expiring.toLowerCase()%> is going to expire! Please use it soon!
                                    </div>
                                    <%
                                        }
                                    %>
                                    <script>
                                        function updateAlert() {
                                            document.location.href = "updateAlert.jsp?expiring=<%=expiring%>";
                                        }
                                    </script>
                                    <table style='width: 100%'>
                                        <tr>
                                            <td style='padding-right:5px;'>
                                                <div class="input-group">
                                                    <span class="input-group-addon">Filter</span>
                                                    <input id="filter" type="text" class="form-control" placeholder="Type here..." value="<%=username%>">
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-primary" onclick="viewAllUsers()">View All Users</button>
                                            </td>
                                        </tr>
                                    </table>


                                    <br/>
                                    <%
                                        String data = (String) request.getAttribute("data");

                                        Gson gson = new Gson();
                                        Type type = new TypeToken<ArrayList<InventoryDataResult>>() {
                                        }.getType();
                                        ArrayList<InventoryDataResult> result = gson.fromJson(data, type);
                                        
                                        
                                        System.out.println(result.get(1).getExpiryDate());

                                        Collections.sort(result, new Comparator<InventoryDataResult>() {
                                            public int compare(InventoryDataResult o1, InventoryDataResult o2) {
                                                return o1.getExpiryDate().compareTo(o2.getExpiryDate());
                                            }
                                        });
                                        System.out.println(result.get(1).getExpiryDate());
                                    %>


                                    <%
                                        String requestMsg = request.getParameter("request");
                                        if (requestMsg != null && requestMsg.equals("success")) {
                                            // Display the success message
                                    %>
                                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <strong>Request Submitted!</strong> You will be notified once he/she allow you to take his/her item!
                                    </div>
                                    <%
                                        }
                                    %>
                                    <div class="first">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><center>Shelf #</center></th>
                                                <th><center>Username</center></th>
                                                <th><center>Food Item</center></th>
                                                <th><center>Food Category</center></th>
                                                <th><center>Expiry Date</center></th>
                                                <th><center>Days to Expiry</center></th>
                                                <th><center>Need It?</center></th>
                                            </tr>
                                        </thead>
                                        <tbody class="searchable">
                                            <%
                                                if (result != null) {
                                                    for (int i = 0; i < result.size(); i++) {
                                                        int daysToExpiry = result.get(i).getDaysToExpiry();
                                                        String style = "";
                                                        if (daysToExpiry <= 3) {
                                                            style = "alert-danger";
                                                        } else if (daysToExpiry <= 7) {
                                                            style = "alert-success";
                                                        } else {
                                                            style = "alert-active";
                                                        }
                                            %>
                                            <tr class="<%=style%>" style="text-align:center;">
                                                <td><%=result.get(i).getShelfId()%></td>
                                                <td><%=result.get(i).getUsername()%></td>
                                                <td><%=result.get(i).getFoodName()%></td>
                                                <td><%=result.get(i).getFoodCategory()%></td>
                                                <td><%=result.get(i).getExpiryDate()%></td>
                                                <td><%=result.get(i).getDaysToExpiry()%></td>
                                                <td>
                                                    <%
                                                        if (!result.get(i).getUsername().equalsIgnoreCase(username) && result.get(i).getDaysToExpiry() <= 3) {
                                                            // Request for food
                                                    %>
                                                    <button type="button" class="btn btn-primary-outline" onclick="location.href = 'requestFood.jsp?username=<%=result.get(i).getUsername()%>&foodName=<%=result.get(i).getFoodName()%>'">Request</button>
                                                    <%
                                                        }
                                                    %>
                                                </td>
                                            </tr>
                                            <%
                                                    }
                                                }
                                            %>
                                        </tbody>
                                    </table>
                                    </div>
                                    <!--MARK-->
                                    
                                    
                                    <div class="second">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><center>Shelf #</center></th>
                                                <th><center>Username</center></th>
                                                <th><center>Food Item</center></th>
                                                <th><center>Food Category</center></th>
                                                <th><center>Expiry Date</center></th>
                                                <th><center>Days to Expiry</center></th>
                                                <th><center>Need It?</center></th>
                                            </tr>
                                        </thead>
                                        <tbody class="searchable">
                                            <%
                                                if (result != null) {
                                                    int size;
                                                    
                                                    ArrayList<InventoryDataResult> tmp = new ArrayList<InventoryDataResult>();

                                                    for(int i = 0; i < result.size(); i++) {
                                                        if(result.get(i).getFoodCategory().equals("Vegetables") || result.get(i).getFoodCategory().equals("Meat") || result.get(i).getFoodCategory().equals("Fish") || result.get(i).getFoodCategory().equals("Fruits")) {
                                                            tmp.add(result.get(i));
                                                        }
                                                    }
                                                    if(tmp.size() > 3) {
                                                        size = 3;
                                                    } else {
                                                        size = tmp.size();
                                                    }
                                                    
                                                    for (int i = 0; i < size; i++) {
                                                        int daysToExpiry = tmp.get(i).getDaysToExpiry();
                                                        String style = "";
                                                        if (daysToExpiry <= 3) {
                                                            style = "alert-danger";
                                                        } else if (daysToExpiry <= 7) {
                                                            style = "alert-success";
                                                        } else {
                                                            style = "alert-active";
                                                        }
                                                        
                                            %>
                                            <tr class="<%=style%>" style="text-align:center;">
                                                <td><%=tmp.get(i).getShelfId()%></td>
                                                <td><%=tmp.get(i).getUsername()%></td>
                                                <td><%=tmp.get(i).getFoodName()%></td>
                                                <td><%=tmp.get(i).getFoodCategory()%></td>
                                                <td><%=tmp.get(i).getExpiryDate()%></td>
                                                <td><%=tmp.get(i).getDaysToExpiry()%></td>
                                                <td>
                                                    <%
                                                        if (!tmp.get(i).getUsername().equalsIgnoreCase(username) && tmp.get(i).getDaysToExpiry() <= 3) {
                                                            // Request for food
                                                    %>
                                                    <button type="button" class="btn btn-primary-outline" onclick="location.href = 'requestFood.jsp?username=<%=result.get(i).getUsername()%>&foodName=<%=result.get(i).getFoodName()%>'">Request</button>
                                                    <%
                                                        }
                                                    %>
                                                </td>
                                            </tr>
                                            <%
                                                    }
                                                }
                                            %>
                                        </tbody>
                                    </table>    
                                    </div>
                                        
                                   
                                    
                                    <div class="third">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><center>Shelf #</center></th>
                                                <th><center>Username</center></th>
                                                <th><center>Food Item</center></th>
                                                <th><center>Food Category</center></th>
                                                <th><center>Expiry Date</center></th>
                                                <th><center>Days to Expiry</center></th>
                                                <th><center>Need It?</center></th>
                                            </tr>
                                        </thead>
                                        <tbody class="searchable">
                                            <%
                                                if (result != null) {
                                                    int size;
                                                    
                                                    ArrayList<InventoryDataResult> tmp = new ArrayList<InventoryDataResult>();

                                                    for(int i = 0; i < result.size(); i++) {
                                                        if(result.get(i).getFoodCategory().equals("Drinks") || result.get(i).getFoodCategory().equals("Dairy Products")) {
                                                            tmp.add(result.get(i));
                                                        }
                                                    }
                                                    if(tmp.size() > 3) {
                                                        size = 3;
                                                    } else {
                                                        size = tmp.size();
                                                    }
                                                    
                                                    
                                                    for (int i = 0; i < size; i++) {
                                                        int daysToExpiry = tmp.get(i).getDaysToExpiry();
                                                        String style = "";
                                                        if (daysToExpiry <= 3) {
                                                            style = "alert-danger";
                                                        } else if (daysToExpiry <= 7) {
                                                            style = "alert-success";
                                                        } else {
                                                            style = "alert-active";
                                                        }
                                                        
                                            %>
                                            <tr class="<%=style%>" style="text-align:center;">
                                                <td><%=tmp.get(i).getShelfId()%></td>
                                                <td><%=tmp.get(i).getUsername()%></td>
                                                <td><%=tmp.get(i).getFoodName()%></td>
                                                <td><%=tmp.get(i).getFoodCategory()%></td>
                                                <td><%=tmp.get(i).getExpiryDate()%></td>
                                                <td><%=tmp.get(i).getDaysToExpiry()%></td>
                                                <td>
                                                    <%
                                                        if (!tmp.get(i).getUsername().equalsIgnoreCase(username) && tmp.get(i).getDaysToExpiry() <= 3) {
                                                            // Request for food
                                                    %>
                                                    <button type="button" class="btn btn-primary-outline" onclick="location.href = 'requestFood.jsp?username=<%=result.get(i).getUsername()%>&foodName=<%=result.get(i).getFoodName()%>'">Request</button>
                                                    <%
                                                        }
                                                    %>
                                                </td>
                                            </tr>
                                            <%
                                                    }
                                                }
                                            %>
                                        </tbody>
                                    </table>    
                                    </div>
                                        
                                    
                                    <div class="fourth">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><center>Shelf #</center></th>
                                                <th><center>Favourite User</center></th>
                                                <th><center>Food Item</center></th>
                                                <th><center>Food Category</center></th>
                                                <th><center>Expiry Date</center></th>
                                                <th><center>Days to Expiry</center></th>
                                                <th><center>Need It?</center></th>
                                            </tr>
                                        </thead>
                                        <tbody class="searchable">
                                            <%
                                                if (result != null) {
                                                    
                                                    ArrayList<Request> list = foodDAO.getMyRequestList(ownerId);
                                                    ArrayList<String> tmpList = new ArrayList<>(); 
                                                    for(Request r : list) {
                                                        tmpList.add(r.getRequester());                                                        
                                                    }
                                                    int occurrences = 0;
                                                    favFriend = "";
                                                    for(String s : tmpList) {
                                                        int tmpOccurrences = Collections.frequency(tmpList, s);
                                                        if(occurrences < tmpOccurrences) {
                                                            occurrences = tmpOccurrences;
                                                            favFriend = s;
                                                        }
                                                    }
                                                    
                                                    Boolean found = false; 
                                                    
                                                    for (int i = 0; i < result.size(); i++) {
                                                        if(!result.get(i).getUsername().equals(favFriend)) {
                                                            continue;
                                                        }
                                                        found = true;
                                                    }
                                                    
                                                    if(found == true) {
                                                    for (int i = 0; i < result.size(); i++) {
                                                        int daysToExpiry = result.get(i).getDaysToExpiry();
                                                        if(!result.get(i).getUsername().equals(favFriend)) {
                                                            continue;
                                                        }
                                                        String style = "";
                                                        if (daysToExpiry <= 3) {
                                                            style = "alert-danger";
                                                        } else if (daysToExpiry <= 7) {
                                                            style = "alert-success";
                                                        } else {
                                                            style = "alert-active";
                                                        }


                                                        
                                            %>
                                            <tr class="<%=style%>" style="text-align:center;">
                                                <td><%=result.get(i).getShelfId()%></td>
                                                <td><%=favFriend%></td>
                                                <td><%=result.get(i).getFoodName()%></td>
                                                <td><%=result.get(i).getFoodCategory()%></td>
                                                <td><%=result.get(i).getExpiryDate()%></td>
                                                <td><%=result.get(i).getDaysToExpiry()%></td>
                                                <td>
                                                    <%
                                                        if (!result.get(i).getUsername().equalsIgnoreCase(username)) {
                                                            // Request for food
                                                    %>
                                                    <button type="button" class="btn btn-primary-outline" onclick="location.href = 'requestFood.jsp?username=<%=result.get(i).getUsername()%>&foodName=<%=result.get(i).getFoodName()%>'">Request</button>
                                                    <%
                                                        }
                                                    %>
                                                </td>
                                            </tr>
                                            <%
                                                        }
                                                    }
                                                }
                                            %>
                                        </tbody>
                                    </table>    
                                    </div>    
                                        
                                    <!-- To uncomment this later -->
                                    <script>
                                        //refreshFilterBox();
                                        
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
