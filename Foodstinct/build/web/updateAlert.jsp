<%-- 
    Document   : updateAlert
    Created on : Apr 20, 2016, 4:47:21 PM
    Author     : gwennisme
--%>

<%@page import="foodstinct.dao.FoodDAO"%>
<%@page import="foodstinct.dao.AlertNotificationDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    int ownerId = (Integer) session.getAttribute("ownerId");
    String expiring = request.getParameter("expiring");
    
    FoodDAO foodDAO = new FoodDAO();
    AlertNotificationDAO alertNotificationDAO = new AlertNotificationDAO();
    
    String[] expiringItems = expiring.split(",");
    if (expiringItems.length > 0) {
        for (int i = 0; i < expiringItems.length; i++) {
            int foodId = foodDAO.retrieveFoodId(expiringItems[i].trim());
            alertNotificationDAO.updateAlert(ownerId, foodId);
        }
    }
    
    response.sendRedirect("/Foodstinct/InventoryServlet?refresh=false");
%>
