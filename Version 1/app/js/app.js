var databaseApp = angular.module('databaseproject', ['ngRoute','ngResource','ngSanitize']);

databaseApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/home', {
        templateUrl: 'partials/home.html',
		controller: 'HomeCtrl'
      }).
      when('/refridgerator', {
        templateUrl: 'partials/refridgerator.html',
        controller: 'RefridgeratorCtrl'
      }).
      otherwise({
        redirectTo: '/home'
      });
  }]);
