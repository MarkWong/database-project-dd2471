databaseApp.controller('HomeCtrl', function ($scope) {
	$scope.username ="";
	$scope.password="";

	$scope.nonmatchingLogin=false;

	$scope.usernameplaceholder = "Enter Username...";
    $scope.passwordplaceholder = "*******";

    $scope.getNonmatchingLogin = function(){
        return $scope.nonmatchingLogin;
    }

    $scope.login = function() {
        window.location="/#refridgerator";
    }

});
